#Load the libraries
import requests
import pandas as pd
import datetime

#Initialized configuration key
parameters = {
        "appid": "7997d82c6674b39cd213174162b90107"
}

#Name of cities which data is going to fetch
ListOfCity = [
              "London", 
              "New York",
              "Paris",
              "Mumbai",
              "New Delhi",
              "Toronto",
              "Rome",
              "Moscow", 
              "Rio", 
              "Melbourne", 
              "Auckland",
              "Dubai", 
              "Cape Town",
              "Madrid", 
              "Tokyo",
              "Seoul", 
              "Amsterdam", 
              "Istanbul",
              "Beijing",
              "Singapore",
              "Delhi",
              "Lucknow",
              "Bengaluru",
              "Chennai",
              "Jaipur"
             ]
#Initialized parameters
latitude = []
longitude = []

# Get lon,lat values
def getLocation(parameters):
    # To handle error
    try:
        for i in range(len(ListOfCity)):
            response = requests.get(f"http://api.openweathermap.org/geo/1.0/direct?q={ListOfCity[i]}", params=parameters)
            response.raise_for_status()
            data = response.json()
            latitude.append(data[0]['lat'])
            longitude.append(data[0]['lon'])
    except Exception as e:
        # print the name of error
        print('Error occured in getLocation function:',e)
getLocation(parameters)

#Initialized air pollutant
AQI,CO,NO,NO2,O3,SO2,PM2_5,PM10,NH3,Date = [],[],[],[],[],[],[],[],[],[]

# Initialized parameters with  defined start and end unix date
parameters2 = {
        "appid": "7997d82c6674b39cd213174162b90107",
        "start": 166840200, # start date 1st Nov,2021 1635739200
        "end": 1668545322, # end date 15th Nov,2022
}

# save dataframe into csv file
def saveCSV(file,City):
    try:
        file.to_csv(City+'_AQI.csv',index=False)
    except Exception as e:
        print('Error occured in saveCSV function:',e)

#function to update individuals city data into csv file
def UpdateCSV(data,City,Latitude,Longitude):
    try:
        for i in range(len(data)):
            print(i)
            AQI.append(data[i]['main']['aqi'])
            CO.append(data[i]['components']['co'])
            NO.append(data[i]['components']['no'])
            NO2.append(data[i]['components']['no2'])
            O3.append(data[i]['components']['o3'])
            SO2.append(data[i]['components']['so2'])
            PM2_5.append(data[i]['components']['pm2_5'])
            PM10.append(data[i]['components']['pm10'])
            NH3.append(data[i]['components']['nh3'])
            Date.append(datetime.datetime.fromtimestamp(data[i]['dt']))
            file = pd.DataFrame(
                {'Date': Date, 
                 'AQI': AQI, 
                 'CO': CO, 
                 'NO': NO,
                 'NO2': NO2,
                 'O3': O3,
                 'SO2': SO2,
                 'PM2_5': PM2_5, 
                 'PM10': PM10, 
                 'NH3': NH3,
                 'City':City,
                 'Latitude':Latitude,
                 'Longitude':Longitude
                }
            )
        return saveCSV(file,City)  
    except Exception as e:
        print('Error occured in UpdateCsv function:',e)

# fetch data of cities using API
def getData():
    try:
        for i in range(len(latitude)):
            response = requests.get(f"http://api.openweathermap.org/data/2.5/air_pollution/history?lat={latitude[i]}&lon={longitude[i]}"
                                 , params=parameters2)
            data = response.json()
            City = ListOfCity[i]
            print(City)
            Latitude = latitude[i]
            Longitude = longitude[i]
            UpdateCSV(data['list'],City,Latitude,Longitude)
    except Exception as e:
         print('Error occured in getData function:',e)

#call function to create csv
getData()
