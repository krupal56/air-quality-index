#load libraries
import pandas as pd

#Read data csv file
df = pd.read_csv('Toronto.csv')

#display first 5 rows
df.head()

# split datetime string into two different date and time column 
df[['Date','Time']] = df.Date.str.split(expand=True)
df.head()

#Group the multiple row data of same date by mean
df_new = df[['Date', 'AQI', 'PM10', 'CO','NO','NO2','O3','SO2','PM2_5','NH3']
           ].groupby(['Date']).mean().sort_values(by = 'AQI')
df_new=df_new.reset_index()
df_new.head()

df_new['City'] = 'Toronto'
df_new['Latitude'] = 43.6534817
df_new['Longitude'] = -79.3839347

df_new.to_csv('Updated_Toronto.csv',index=False)
